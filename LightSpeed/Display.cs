﻿using LightSpeed.GameObjects;
using LightSpeed.GameObjects.Animated;
using LightSpeed.Logic;
using LightSpeed.Model;
using LightSpeed.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace LightSpeed
{
    class Display : FrameworkElement
    {
        private IGameLogic logic;
        private IGameModel model;
        private MenuView menuview;
        private GameView gameview;
        private TradeView tradeview;
        private GameOverView gameover;
        private VictoryView victory;

        private View CurrentView { get; set; }

        /// <summary>
        /// Initializes the display object
        /// </summary>
        /// <param name="logic">Logic interface</param>
        /// <param name="model">Model interface</param>
        public void Initialize(IGameLogic logic, IGameModel model)
        {
            this.logic = logic;
            this.model = model;
            this.model.StateChanged += Model_StateChanged;
            this.logic.Message += Logic_Message;
            this.UpdateVU();

            Window w = Window.GetWindow(this);
            w.KeyDown += Display_KeyDown;
            w.MouseDown += Display_MouseDown;
            w.MouseMove += Display_MouseMove;

            this.menuview = new MenuView(logic, model);
            this.gameview = new GameView(logic, model);
            this.tradeview = new TradeView(logic, model);
            this.gameover = new GameOverView(logic, model);
            this.victory = new VictoryView(logic, model);

            this.Model_StateChanged(this, null);
        }

        private void Logic_Message(object sender, MessageEventArgs e)
        {
            e.Response = MessageBox.Show(e.Message, e.Title, e.Choice ? MessageBoxButton.YesNo : MessageBoxButton.OK) == MessageBoxResult.Yes;
        }

        private void Model_StateChanged(object sender, EventArgs e)
        {
            this.CurrentView = this.GenerateView(this.model.State);
        }

        private View GenerateView(GameStates state)
        {
            switch (state)
            {
                case GameStates.Menu:
                    return this.menuview;
                case GameStates.Game:
                case GameStates.Combat:
                    return this.gameview;
                case GameStates.Trade:
                    return this.tradeview;
                case GameStates.GameOver:
                    return this.gameover;
                case GameStates.Victory:
                    return this.victory;
                default:
                    return this.menuview;
            }
        }

        /// <summary>
        /// Update View utility dimensions
        /// </summary>
        public void UpdateVU()
        {
            double w = this.ActualHeight * 1.77777;
            this.Width = w;
            ViewUtility.Width = w;
            ViewUtility.Height = this.ActualHeight;
        }

        /// <summary>
        /// Step animations forward, remove if finished
        /// </summary>
        public void Tick()
        {
            List<AnimatedGameObject> toRemove = new List<AnimatedGameObject>();
            foreach (GameObject o in this.CurrentView.GameObjects)
            {
                if (o is AnimatedGameObject a)
                {
                    a.Tick();
                    if (a.Finished)
                    {
                        toRemove.Add(a);
                    }
                }
            }
            foreach (AnimatedGameObject a in toRemove)
            {
                if (a is Torpedo t)
                {
                    this.logic.PlayerAttack(true);
                    this.CurrentView.GameObjects.Add(new Explosion(t.Endpoint.X, t.Endpoint.Y, 150));
                }
                this.CurrentView.GameObjects.Remove(a);
            }
        }

        /// <summary>
        /// Render current view
        /// </summary>
        /// <param name="drawingContext">Drawing context</param>
        protected override void OnRender(DrawingContext drawingContext)
        {
            base.OnRender(drawingContext);

            drawingContext.DrawRectangle(Brushes.Transparent, null, new Rect(0, 0, this.ActualWidth, this.ActualHeight));
            // drawingContext.DrawRectangle(Brushes.Magenta, null, new Rect(this.ActualWidth / 2 - 2, 0, 2, this.ActualHeight));
            if (this.logic != null)
            {
                this.CurrentView.Render(drawingContext);
            }
        }

        private void Display_MouseMove(object sender, MouseEventArgs e)
        {
            this.CurrentView.HandleMouseMove(e.GetPosition(this));
        }

        private void Display_MouseDown(object sender, MouseButtonEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine(e.GetPosition(this));
            this.CurrentView.HandleMouseDown(e);
        }

        private void Display_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.NumPad0)
            {
                Window.GetWindow(this).Width = 1920;
            }
            this.CurrentView.HandleKey(e);
        }
    }
}
