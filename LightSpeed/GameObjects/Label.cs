﻿using LightSpeed.Views;
using System;
using System.Windows;
using System.Windows.Media;

namespace LightSpeed.GameObjects
{
    /// <summary>
    /// Label game object
    /// </summary>
    class Label : GameObject
    {
        /// <summary>
        /// Text of label
        /// </summary>
        public Func<string> Content { get; set; }

        /// <summary>
        /// Color of text
        /// </summary>
        public Brush Color { get; set; }

        /// <summary>
        /// Size of text
        /// </summary>
        public int Size { get; set; }

        /// <summary>
        /// Style of text
        /// </summary>
        public FontStyle Style { get; set; }

        /// <summary>
        /// Weight of text
        /// </summary>
        public FontWeight Weight { get; set; }

        /// <summary>
        /// Is centered
        /// </summary>
        public bool IsCentered { get; set; }

        /// <summary>
        /// Width of space if centered
        /// </summary>
        public double Width { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        /// <param name="content">Text of label</param>
        /// <param name="color">Color of text</param>
        /// <param name="size">Size of text</param>
        /// <param name="style">Style of text</param>
        /// <param name="weight">Weight of text</param>
        public Label(double x, double y, Func<string> content, Brush color, int size, FontStyle style, FontWeight weight) : base(x, y)
        {
            this.Content = content;
            this.Position = new Point(x, y);
            this.Color = color;
            this.Size = size;
            this.Style = style;
            this.Weight = weight;
        }

        /// <summary>
        /// Constructor if centered
        /// </summary>
        /// <param name="y">Y position</param>
        /// <param name="content">Text of label</param>
        /// <param name="color">Color of text</param>
        /// <param name="size">Size of text</param>
        /// <param name="style">Style of text</param>
        /// <param name="weight">Weight of text</param>
        /// <param name="x">Inset X position</param>
        /// <param name="width">Width of available space</param>
        public Label(double y, Func<string> content, Brush color, int size, FontStyle style, FontWeight weight, double x = 0, double width = 1920) : base(x, y)
        {
            this.Content = content;
            this.IsCentered = true;
            this.Color = color;
            this.Size = size;
            this.Style = style;
            this.Weight = weight;
            this.Width = width;
        }

        /// <inheritdoc />
        public override void Render(DrawingContext ctx)
        {
            double scale = ViewUtility.Scale;
            FormattedText format = new FormattedText(
                this.Content(),
                System.Globalization.CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface(new FontFamily("Verdana"), this.Style, this.Weight, FontStretches.Normal),
                this.Size * scale,
                this.Color
            );

            Point p = new Point(IsCentered ? (this.ScaledPos.X + (this.Width * scale - format.Width) / 2) : this.ScaledPos.X, this.ScaledPos.Y);
            ctx.DrawText(format, p);
        }
    }
}
