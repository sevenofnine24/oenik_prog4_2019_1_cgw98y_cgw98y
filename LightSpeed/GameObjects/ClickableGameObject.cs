﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace LightSpeed.GameObjects
{
    class ClickableGameObject : GameObject
    {
        /// <summary>
        /// Is mouse on top
        /// </summary>
        public bool Hover { get; set; }

        /// <summary>
        /// Hitbox of object
        /// </summary>
        public Rect Hitbox { get; protected set; }

        /// <summary>
        /// Action when clicked
        /// </summary>
        public Action<MouseButtonEventArgs> Clicked { get; protected set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        /// <param name="clicked">Clicked action</param>
        public ClickableGameObject(double x, double y, Action<MouseButtonEventArgs> clicked) : base(x, y)
        {
            this.Hover = false;
            this.Clicked = clicked;
        }
    }
}
