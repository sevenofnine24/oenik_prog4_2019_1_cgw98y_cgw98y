﻿using LightSpeed.Views;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace LightSpeed.GameObjects
{
    /// <summary>
    /// Basic rectangle object
    /// </summary>
    class Rectangle : ClickableGameObject
    {
        /// <summary>
        /// Size of rectangle
        /// </summary>
        public Size Size { get; set; }

        /// <summary>
        /// Fill color of rectangle
        /// </summary>
        public Brush Fill { get; set; }

        /// <summary>
        /// Border color of rectangle
        /// </summary>
        public Brush Stroke { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        /// <param name="width">Width of rectangle</param>
        /// <param name="height">Height of rectangle</param>
        /// <param name="fill">Fill color of rectangle</param>
        /// <param name="clicked">Clicked action</param>
        /// <param name="stroke">Border color of rectangle</param>
        public Rectangle(double x, double y, double width, double height, Brush fill, Action<MouseButtonEventArgs> clicked = null, Brush stroke = null) : base(x, y, clicked)
        {
            this.Size = new Size(width, height);
            this.Fill = fill;
            this.Stroke = stroke;
        }

        /// <inheritdoc />
        public override void Render(DrawingContext ctx)
        {
            double scale = ViewUtility.Scale;
            Pen p = this.Stroke == null ? null : new Pen(this.Stroke, 3);

            this.Hitbox = new Rect(this.Position.X * scale, this.Position.Y * scale, this.Size.Width * scale, this.Size.Height * scale);
            ctx.DrawRectangle(this.Fill, p, this.Hitbox);
        }
    }
}
