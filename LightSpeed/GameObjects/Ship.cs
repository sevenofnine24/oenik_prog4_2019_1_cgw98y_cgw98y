﻿using LightSpeed.Views;
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace LightSpeed.GameObjects
{
    /// <summary>
    /// Ship object (player or hostile)
    /// </summary>
    class Ship : GameObject
    {
        private readonly Brush shieldblue;
        
        /// <summary>
        /// Image of ship
        /// </summary>
        public BitmapImage Image { get; set; }

        /// <summary>
        /// Is shielded (dynamic)
        /// </summary>
        public Func<bool> Shields { get; set; }

        /// <summary>
        /// Width of ship (height calculated)
        /// </summary>
        public double Width { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        /// <param name="width">Width of ship</param>
        /// <param name="image">Image of ship</param>
        /// <param name="shields">Is shielded (dynamic)</param>
        public Ship(double x, double y, double width, Uri image, Func<bool> shields) : base(x, y)
        {
            this.Width = width;
            this.Image = new BitmapImage(image);
            this.Shields = shields;

            ColorConverter cc = new ColorConverter();
            this.shieldblue = new RadialGradientBrush((Color)cc.ConvertFrom("#229ACAFF"), (Color)cc.ConvertFrom("#66007AFF"));
        }

        /// <inheritdoc />
        public override void Render(DrawingContext ctx)
        {
            base.Render(ctx);
            double scale = ViewUtility.Scale;

            Rect r = new Rect(this.ScaledPos.X, this.ScaledPos.Y, this.Width * scale, this.Width * scale * (this.Image.Height / this.Image.Width));
            ctx.DrawImage(this.Image, r);

            if (this.Shields != null && this.Shields())
            {
                ctx.DrawEllipse(this.shieldblue, null, new Point(r.X + r.Width / 2, r.Y + r.Height / 2), r.Width / 1.6, r.Height / 1.6);
            }
        }
    }
}
