﻿using LightSpeed.Views;
using System;
using System.Windows;
using System.Windows.Media;

namespace LightSpeed.GameObjects
{
    class GameObject
    {
        /// <summary>
        /// Scaled position for easy access
        /// </summary>
        protected Point ScaledPos => new Point(this.Position.X * ViewUtility.Scale, this.Position.Y * ViewUtility.Scale);

        /// <summary>
        /// Position of object
        /// </summary>
        public Point Position { get; set; }

        /// <summary>
        /// Is object visible
        /// </summary>
        public Func<bool> Visible { get; set; }

        /// <summary>
        /// Constructor for game object
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        public GameObject(double x, double y)
        {
            this.Position = new Point(x, y);
        }

        /// <summary>
        /// Render object
        /// </summary>
        /// <param name="ctx">Drawing context</param>
        public virtual void Render(DrawingContext ctx)
        {
            // empty
        }
    }
}
