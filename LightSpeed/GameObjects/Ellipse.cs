﻿using LightSpeed.Views;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace LightSpeed.GameObjects
{
    class Ellipse : Rectangle
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">X position</param>
        /// <param name="radiusx">Radius of ellipse (X)</param>
        /// <param name="radiusy">Radius of ellipse (Y)</param>
        /// <param name="fill">Fill of ellipse</param>
        /// <param name="clicked">Clicked action</param>
        public Ellipse(double x, double y, double radiusx, double radiusy, Brush fill, Action<MouseButtonEventArgs> clicked = null) : base(x, y, radiusx, radiusy, fill, clicked)
        {
            // nothing to do here
        }

        /// <inheritdoc />
        public override void Render(DrawingContext ctx)
        {
            base.Render(ctx);

            double scale = ViewUtility.Scale;
            ctx.DrawEllipse(this.Fill, null, new Point(this.Position.X * scale, this.Position.Y * scale), this.Size.Width * scale, this.Size.Height * scale);
        }
    }
}
