﻿using LightSpeed.Views;
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace LightSpeed.GameObjects.Animated
{
    /// <summary>
    /// Phaser game object
    /// </summary>
    class Phaser : AnimatedGameObject
    {
        private readonly BitmapImage phaser;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        /// <param name="player">Is player's phaser</param>
        public Phaser(double x, double y, bool player) : base(x, y)
        {
            Uri u = new Uri("pack://application:,,,/LightSpeed;component/Assets/" + (player ? "phaserbeam.png" : "krillphaser.png"));
            this.phaser = new BitmapImage(u);
        }

        /// <inheritdoc />
        public override void Tick()
        {
            base.Tick();

            if (this.timer == 75)
            {
                this.Finished = true;
            }
        }

        /// <inheritdoc />
        public override void Render(DrawingContext ctx)
        {
            base.Render(ctx);

            double scale = ViewUtility.Scale;
            double width = 600 * scale;
            Size s = new Size(
                width,
                width * (this.phaser.Height / this.phaser.Width)
            );
            ctx.DrawImage(this.phaser, new Rect(this.ScaledPos, s));
        }
    }
}
