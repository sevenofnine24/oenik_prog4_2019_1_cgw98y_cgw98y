﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace LightSpeed.GameObjects.Animated
{
    /// <summary>
    /// Explosion object
    /// </summary>
    class Explosion : AnimatedGameObject
    {
        private readonly BitmapImage explosion;
        private readonly double width;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        /// <param name="width">Width of explosion</param>
        public Explosion(double x, double y, double width) : base(x, y)
        {
            this.explosion = new BitmapImage(new Uri("pack://application:,,,/LightSpeed;component/Assets/explosion.png"));
            this.width = width;
        }

        /// <inheritdoc />
        public override void Tick()
        {
            base.Tick();

            if (this.timer == 75)
            {
                this.Finished = true;
            }
        }

        /// <inheritdoc />
        public override void Render(DrawingContext ctx)
        {
            base.Render(ctx);

            Size s = new Size(
                this.width,
                this.width * (this.explosion.Height / this.explosion.Width)
            );
            ctx.DrawImage(this.explosion, new Rect(this.ScaledPos, s));
        }
    }
}
