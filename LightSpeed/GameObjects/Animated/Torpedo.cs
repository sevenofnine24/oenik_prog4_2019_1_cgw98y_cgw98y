﻿using LightSpeed.Views;
using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace LightSpeed.GameObjects.Animated
{
    class Torpedo : AnimatedGameObject
    {
        private Point current;
        private readonly BitmapImage torpedo;
        private readonly int distanceX;
        private readonly int distanceY;

        /// <summary>
        /// Endpoint of torpedo
        /// </summary>
        public Point Endpoint => new Point(this.Position.X + this.distanceX, this.Position.Y - this.distanceY);

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        /// <param name="distanceX">Distance to travel in X direction</param>
        /// <param name="distanceY">Distance to travel in Y direction</param>
        public Torpedo(double x, double y, int distanceX, int distanceY) : base(x, y)
        {
            Random r = new Random();
            this.distanceX = distanceX + r.Next(-50, 51);
            this.distanceY = distanceY + r.Next(-50, 51);

            this.torpedo = new BitmapImage(new Uri("pack://application:,,,/LightSpeed;component/Assets/torpedo.png"));
        }

        /// <inheritdoc />
        public override void Tick()
        {
            base.Tick();
            current = new Point(
                this.Position.X + ((double)this.timer / 150) * this.distanceX,
                this.Position.Y - ((double)this.timer / 150) * this.distanceY
            );

            if (this.timer == 150)
            {
                this.Finished = true;
            }
        }

        /// <inheritdoc />
        public override void Render(DrawingContext ctx)
        {
            base.Render(ctx);

            double scale = ViewUtility.Scale;
            double width = 150 * scale;
            Rect r = new Rect(
                this.current.X * scale,
                this.current.Y * scale,
                width,
                width * (this.torpedo.Height / this.torpedo.Width)
            );
            ctx.DrawImage(this.torpedo, r);
        }
    }
}
