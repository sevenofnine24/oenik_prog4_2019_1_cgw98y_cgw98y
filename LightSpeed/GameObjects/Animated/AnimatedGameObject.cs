﻿
namespace LightSpeed.GameObjects.Animated
{
    /// <summary>
    /// Animated (timed) game object
    /// </summary>
    class AnimatedGameObject : GameObject
    {
        protected int timer;

        /// <summary>
        /// Is the animation finished
        /// </summary>
        public bool Finished { get; protected set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        public AnimatedGameObject(double x, double y) : base(x, y)
        {
            this.timer = 0;
            this.Finished = false;
        }

        /// <summary>
        /// Advance timer
        /// </summary>
        public virtual void Tick()
        {
            timer++;
        }
    }
}
