﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Media;
using LightSpeed.Views;

namespace LightSpeed.GameObjects
{
    /// <summary>
    /// Available energy game object
    /// </summary>
    class AvailableEnergy : GameObject
    {
        /// <summary>
        /// Available dynamic variable
        /// </summary>
        public Func<int> Available { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        /// <param name="available">Available energy</param>
        public AvailableEnergy(double x, double y, Func<int> available) : base(x, y)
        {
            this.Available = available;
        }

        /// <inheritdoc />
        public override void Render(DrawingContext ctx)
        {
            base.Render(ctx);
            double scale = ViewUtility.Scale;
            double size = 150 * scale;

            FormattedText avail = new FormattedText("available", CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Verdana"), 20 * scale, Brushes.White);
            FormattedText count = new FormattedText(this.Available().ToString(), CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Verdana"), 40 * scale, Brushes.White);

            ctx.DrawText(avail, new Point(this.ScaledPos.X + (size - avail.Width) / 2, this.ScaledPos.Y + size - avail.Height * 1.5));
            ctx.DrawText(count, ViewUtility.CenterofRect(new Rect(this.ScaledPos.X, this.ScaledPos.Y, size, size), count.Width, count.Height));
                
        }
    }
}
