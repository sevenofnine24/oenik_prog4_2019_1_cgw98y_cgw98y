﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using LightSpeed.Views;

namespace LightSpeed.GameObjects
{
    /// <summary>
    /// Button game object
    /// </summary>
    class Button : ClickableGameObject
    {
        private readonly Brush hoverbrush;

        /// <summary>
        /// Fill of button
        /// </summary>
        public Brush Fill { get; set; }

        /// <summary>
        /// Color of text inside
        /// </summary>
        public Brush Color { get; set; }

        /// <summary>
        /// Size of button
        /// </summary>
        public Size Size { get; set; }

        /// <summary>
        /// Size of text inside
        /// </summary>
        public int FontSize { get; set; }

        /// <summary>
        /// Text inside button (dynamic)
        /// </summary>
        public Func<string> Content { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        /// <param name="width">Width of button</param>
        /// <param name="height">Height of button</param>
        /// <param name="content">Text in button</param>
        /// <param name="clicked">Clicked action</param>
        public Button(double x, double y, double width, double height, Func<string> content, Action<MouseButtonEventArgs> clicked = null) : base(x, y, clicked)
        {
            this.FontSize = 16;
            this.Size = new Size(width, height);
            this.Color = Brushes.White;
            this.Content = content;

            BrushConverter cv = new BrushConverter();
            this.hoverbrush = (Brush)cv.ConvertFrom("#44FFFFFF");
            this.Fill = (Brush)cv.ConvertFrom("#FF007AFF");
        }

        /// <inheritdoc />
        public override void Render(DrawingContext ctx)
        {
            double scale = ViewUtility.Scale;
            this.Hitbox = new Rect(this.ScaledPos.X, this.ScaledPos.Y, this.Size.Width * scale, this.Size.Height * scale);
            ctx.DrawRectangle(this.Fill, null, this.Hitbox);

            if (this.Hover)
            {
                ctx.DrawRectangle(this.hoverbrush, null, this.Hitbox);
            }

            FormattedText format = new FormattedText(this.Content(),
                System.Globalization.CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface(new FontFamily("Verdana"), FontStyles.Normal, FontWeights.Bold, FontStretches.Normal),
                this.FontSize * scale,
                this.Color
            );
            ctx.DrawText(format, ViewUtility.CenterofRect(this.Hitbox, format.Width, format.Height));
        }
    }
}
