﻿using LightSpeed.Views;
using System;
using System.Windows;
using System.Windows.Media;

namespace LightSpeed.GameObjects
{
    /// <summary>
    /// Progress bar object
    /// </summary>
    class ProgressBar : GameObject
    {
        /// <summary>
        /// Size of progress bar
        /// </summary>
        public Size Size { get; set; }

        /// <summary>
        /// Value shown (dynamic)
        /// </summary>
        public Func<int> Value { get; set; }

        /// <summary>
        /// Background of progress bar
        /// </summary>
        public Brush Fill { get; set; }

        /// <summary>
        /// Color of value
        /// </summary>
        public Brush Color { get; set; }

        /// <summary>
        /// Color of label
        /// </summary>
        public Brush Foreground { get; set; }

        /// <summary>
        /// Content of label
        /// </summary>
        public string Label { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        /// <param name="width">Width of progress bar</param>
        /// <param name="height">Height of progress bar</param>
        /// <param name="value">Value shown</param>
        /// <param name="color">Color of value shown</param>
        /// <param name="label">Label to show (if any)</param>
        public ProgressBar(double x, double y, double width, double height, Func<int> value, Brush color, string label = null) : base(x, y)
        {
            this.Position = new Point(x, y);
            this.Size = new Size(width, height);
            this.Value = value;
            this.Fill = Brushes.DarkGray;
            this.Color = color;
            this.Foreground = Brushes.White;
            this.Label = label;
        }

        /// <inheritdoc />
        public override void Render(DrawingContext ctx)
        {
            double scale = ViewUtility.Scale;
            int val = this.Value();

            Rect bar = new Rect(this.Position.X * scale, this.Position.Y * scale, this.Size.Width * scale, this.Size.Height * scale);
            ctx.DrawRectangle(this.Fill, null, bar);

            Rect valr = new Rect(bar.X, bar.Y, bar.Width * ((double)val / 100), bar.Height);
            ctx.DrawRectangle(this.Color, null, valr);

            FormattedText text = new FormattedText(
                this.Label ?? val.ToString(),
                System.Globalization.CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface("Verdana Bold"),
                bar.Height / 1.75,
                this.Foreground
            );
            ctx.DrawText(text, ViewUtility.CenterofRect(bar, text.Width, text.Height));
        }
    }
}
