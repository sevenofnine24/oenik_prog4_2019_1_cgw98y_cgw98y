﻿using LightSpeed.Views;
using System;
using System.Globalization;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace LightSpeed.GameObjects
{
    /// <summary>
    /// Energy display object
    /// </summary>
    class EnergyDisplay : ClickableGameObject
    {
        private readonly Brush blue;
        private readonly Brush bg;

        /// <summary>
        /// Dynamic current level variable
        /// </summary>
        public Func<string, int> Level { get; set; }

        /// <summary>
        /// Maximum level
        /// </summary>
        public int Max { get; private set; }

        /// <summary>
        /// Text below energy display
        /// </summary>
        public string Subtext { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="x">X position</param>
        /// <param name="y">Y position</param>
        /// <param name="level">Current level</param>
        /// <param name="max">Maximum level</param>
        /// <param name="subtext">Text below level</param>
        /// <param name="clicked">Clicked action</param>
        public EnergyDisplay(double x, double y, Func<string,int> level, int max, string subtext, Action<MouseButtonEventArgs> clicked) : base(x + 6, y - 6, clicked)
        {
            this.Level = level;
            this.Max = max;
            this.Subtext = subtext;
            BrushConverter bc = new BrushConverter();
            this.blue = (Brush)bc.ConvertFrom("#FF007AFF");
            this.bg = (Brush)bc.ConvertFrom("#44469FFF");
            double scale = ViewUtility.Scale;
            this.Hitbox = new Rect(this.ScaledPos.X, this.ScaledPos.Y, 150 * scale, 150 * scale);
        }

        /// <inheritdoc />
        public override void Render(DrawingContext ctx)
        {
            base.Render(ctx);
            double scale = ViewUtility.Scale;
            double size = 144 * scale;

            if(this.Hover)
            {
                ctx.DrawRectangle(this.bg, new Pen(this.blue, 1), new Rect(this.ScaledPos.X, this.ScaledPos.Y, size, size));
            }

            FormattedText text = new FormattedText(this.Subtext, CultureInfo.CurrentCulture, FlowDirection.LeftToRight, new Typeface("Verdana"), 20 * scale, Brushes.White);
            double spacing = 5 * scale;
            double availablespace = size - spacing * 2 - text.Height;
            double perrect = availablespace / this.Max;

            double width = 84 * scale;
            double centerspace = 30 * scale;
            int level = this.Level(this.Subtext);
            for (int i = this.Max - 1; i >= 0; i--)
            {
                if (level > 0)
                {
                    ctx.DrawRectangle(this.blue, null, new Rect(this.ScaledPos.X + centerspace, this.ScaledPos.Y + perrect * i + spacing, width, perrect - spacing));
                    level--;
                }
            }
            ctx.DrawText(text, new Point(this.ScaledPos.X + (size - text.Width) / 2, this.ScaledPos.Y + size - text.Height));
        }
    }
}
