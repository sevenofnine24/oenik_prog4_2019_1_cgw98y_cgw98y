﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LightSpeed.Views
{
    /// <summary>
    /// View utility class
    /// </summary>
    public static class ViewUtility
    {
        /// <summary>
        /// Width of main window
        /// </summary>
        public static double Width { get; set; }

        /// <summary>
        /// Height of main window
        /// </summary>
        public static double Height { get; set; }

        /// <summary>
        /// Scale of main window (full HD is default)
        /// </summary>
        public static double Scale => Height / 1080;


        /// <summary>
        /// Calculate center of space
        /// </summary>
        /// <param name="space">Width of space</param>
        /// <param name="size">Size of object</param>
        /// <returns>Calculated position</returns>
        public static double Center(double space, double size)
        {
            return space / 2 - size / 2;
        }

        /// <summary>
        /// Positions object
        /// at center of rectangle
        /// </summary>
        /// <param name="r">Rectangle</param>
        /// <param name="width">Width of object</param>
        /// <param name="height">Height of object</param>
        /// <returns>Calculated position</returns>
        public static Point CenterofRect(Rect r, double width, double height)
        {
            return new Point(
                r.X + (r.Width - width) / 2,
                r.Y + (r.Height - height) / 2
            );
        }
    }
}
