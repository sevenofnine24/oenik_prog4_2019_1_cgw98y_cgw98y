﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using LightSpeed.GameObjects;
using LightSpeed.Logic;
using LightSpeed.Model;

namespace LightSpeed.Views
{
    class PausableView : View
    {
        private bool pausemenu;
        public readonly List<GameObject> PauseMenu;

        /// <inheritdoc />
        public PausableView(IGameLogic logic, IGameModel model) : base(logic, model)
        {
            BrushConverter bc = new BrushConverter();

            this.PauseMenu = new List<GameObject>();
            Rectangle bg = new Rectangle(0, 0, 1920, 1080, (Brush)bc.ConvertFrom("#AA000000"));
            Label title = new Label(270, () => "Pause menu", Brushes.LightGray, 30, FontStyles.Normal, FontWeights.Bold);
            Button save = new Button(ViewUtility.Center(1920, 250), 390, 250, 60, () => "Save", this.Save);
            Button menu = new Button(ViewUtility.Center(1920, 250), 490, 250, 60, () => "Return to menu", this.Return);

            this.PauseMenu.AddRange(new GameObject[] { bg, title, save, menu });
        }

        /// <inheritdoc />
        public override void HandleKey(KeyEventArgs e)
        {
            base.HandleKey(e);
            if (e.Key == Key.Escape)
            {
                if (this.pausemenu)
                {
                    this.logic.TogglePause(false);
                }
                else
                {
                    this.logic.TogglePause(true);
                }
                this.pausemenu = !this.pausemenu;
            }
        }

        

        /// <inheritdoc />
        public override void Render(DrawingContext ctx)
        {
            base.Render(ctx);

            if (this.pausemenu)
            {
                foreach (GameObject o in this.PauseMenu)
                {
                    o.Render(ctx);
                }
            }
        }

        /// <summary>
        /// Displays player's HUD
        /// </summary>
        protected void PlayerHUD()
        {
            this.GameObjects.Add(new GameObjects.Ship(150, 480, 640, new Uri("pack://application:,,,/LightSpeed;component/Assets/orvillenobg.png"), () => this.model.PlayerShip.ShieldCharge == 100));
            this.GameObjects.Add(new ProgressBar(50, 50, 500, 50, () => this.model.PlayerShip.Health, Brushes.Green));
            this.GameObjects.Add(new Label(62, () => "Sector: " + this.model.Sector, Brushes.White, 24, FontStyles.Normal, FontWeights.Bold, 570, 150));
            this.GameObjects.Add(new Label(62, () => "Fuel: " + this.model.Fuel, Brushes.White, 24, FontStyles.Normal, FontWeights.Bold, 720, 150));
            this.GameObjects.Add(new Label(62, () => "Scrap: " + this.model.Scrap, Brushes.White, 24, FontStyles.Normal, FontWeights.Bold, 870, 150));
            this.GameObjects.Add(new Label(62, () => "Damage: " + this.model.WeaponsMultiplier + "x", Brushes.White, 24, FontStyles.Normal, FontWeights.Bold, 1020, 250));
            this.GameObjects.Add(new Label(62, () => "Shield charge: " + this.model.ShieldMultiplier + "x", Brushes.White, 24, FontStyles.Normal, FontWeights.Bold, 1270, 250));

            // jump and torpedo
            Button jump = new Button(710, 980, 200, 50, () => "Jump", e => this.logic.JumpSector())
            {
                Fill = Brushes.Transparent
            };
            this.GameObjects.Add(new ProgressBar(710, 980, 200, 50, () => this.model.FtlCharge, Brushes.Orange, ""));
            this.GameObjects.Add(jump);
            this.GameObjects.Add(new Button(1010, 980, 200, 50, () => "Torpedo (" + this.model.TorpedoCount + ")"));
        }

        private void Save(MouseEventArgs e)
        {
            this.logic.ReturnToMenu();
        }

        private void Return(MouseEventArgs e)
        {
            this.logic.ReturnToMenu();
        }

        private bool Visible()
        {
            return this.pausemenu;
        }
    }
}
