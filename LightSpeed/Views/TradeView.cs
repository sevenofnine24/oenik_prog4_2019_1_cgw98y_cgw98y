﻿using LightSpeed.Logic;
using LightSpeed.Model;
using LightSpeed.GameObjects;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;

namespace LightSpeed.Views
{
    /// <summary>
    /// Trader (shop) view
    /// </summary>
    class TradeView : PausableView
    {
        private string cheat = "SCRAP";
        private int cheatcount;

        /// <inheritdoc />
        public TradeView(IGameLogic logic, IGameModel model) : base(logic, model)
        {
            this.PlayerHUD();

            this.GameObjects.Add(new Rectangle(1170, 240, 540, 600, Brushes.White, null, new SolidColorBrush(Color.FromArgb(255, 0, 122, 255))));
            this.GameObjects.Add(new Label(260, () => "Shop", Brushes.Black, 32, FontStyles.Normal, FontWeights.Bold, 1190, 500));
            this.GameObjects.Add(new Label(1190, 312, () => "Fuel - 1s", Brushes.Black, 24, FontStyles.Normal, FontWeights.Bold));
            this.GameObjects.Add(new Label(1190, 364, () => "Torpedo - 2s", Brushes.Black, 24, FontStyles.Normal, FontWeights.Bold));
            this.GameObjects.Add(new Label(1190, 416, () => "Weapons upgrade - 10s", Brushes.Black, 24, FontStyles.Normal, FontWeights.Bold));
            this.GameObjects.Add(new Label(1190, 468, () => "Shield upgrade - 20s", Brushes.Black, 24, FontStyles.Normal, FontWeights.Bold));

            this.GameObjects.Add(new Button(1470, 312, 100, 30, () => "Buy", e => this.logic.BuyFuel()));
            this.GameObjects.Add(new Button(1590, 312, 100, 30, () => "Sell", e => this.logic.SellFuel()));
            this.GameObjects.Add(new Button(1470, 364, 100, 30, () => "Buy", e => this.logic.BuyTorpedo()));
            this.GameObjects.Add(new Button(1590, 364, 100, 30, () => "Sell", e => this.logic.SellTorpedo()));
            this.GameObjects.Add(new Button(1590, 416, 100, 30, () => "Buy", e => this.logic.BuyWeapon()));
            this.GameObjects.Add(new Button(1590, 468, 100, 30, () => "Buy", e => this.logic.BuyShield()));
        }

        /// <inheritdoc />
        public override void HandleKey(KeyEventArgs e)
        {
            base.HandleKey(e);

            if (cheat[cheatcount] == e.Key.ToString()[0])
            {
                cheatcount++;
                if (cheatcount == cheat.Length)
                {
                    cheatcount = 0;
                    this.model.Scrap += 100;
                }
            }
            else
            {
                cheatcount = 0;
            }
        }
    }
}
