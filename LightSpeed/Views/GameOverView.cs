﻿using System.Windows;
using System.Windows.Media;
using LightSpeed.GameObjects;
using LightSpeed.Logic;
using LightSpeed.Model;

namespace LightSpeed.Views
{
    /// <summary>
    /// Game over view
    /// </summary>
    class GameOverView : View
    {
        /// <inheritdoc />
        public GameOverView(IGameLogic logic, IGameModel model) : base(logic, model)
        {
            this.GameObjects.Add(new Label(270, () => "Game Over", Brushes.White, 42, FontStyles.Normal, FontWeights.Bold));
            this.GameObjects.Add(new Label(390, () => "You died.", Brushes.White, 32, FontStyles.Normal, FontWeights.Normal));
            this.GameObjects.Add(new Button(ViewUtility.Center(1920, 250), 490, 250, 60, () => "Return to main menu", e => this.logic.ReturnToMenu()));
            this.GameObjects.Add(new Button(ViewUtility.Center(1920, 250), 590, 250, 60, () => "Exit", e => Application.Current.Shutdown(0)));
        }
    }
}
