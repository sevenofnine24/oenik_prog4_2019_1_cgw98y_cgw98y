﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using LightSpeed.GameObjects;
using LightSpeed.Logic;
using LightSpeed.Model;

namespace LightSpeed.Views
{
    /// <summary>
    /// Victory view
    /// </summary>
    class VictoryView : View
    {
        /// <inheritdoc />
        public VictoryView(IGameLogic logic, IGameModel model) : base(logic, model)
        {
            this.GameObjects.Add(new Label(270, () => "Victory", Brushes.White, 42, FontStyles.Normal, FontWeights.Bold));
            this.GameObjects.Add(new Label(390, () => "You have defeated the Krill.", Brushes.White, 32, FontStyles.Normal, FontWeights.Normal));
            this.GameObjects.Add(new Button(ViewUtility.Center(1920, 250), 490, 250, 60, () => "Return to main menu", e => this.logic.ReturnToMenu()));
            this.GameObjects.Add(new Button(ViewUtility.Center(1920, 250), 590, 250, 60, () => "Exit", e => Application.Current.Shutdown(0)));
        }
    }
}
