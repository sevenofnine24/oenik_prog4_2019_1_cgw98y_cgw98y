﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using LightSpeed.GameObjects;
using LightSpeed.GameObjects.Animated;
using LightSpeed.Logic;
using LightSpeed.Model;

namespace LightSpeed.Views
{
    /// <summary>
    /// Main game view
    /// </summary>
    class GameView : PausableView
    {
        private string cheat = "TORP";
        private int cheatcount;

        /// <inheritdoc />
        public GameView(IGameLogic logic, IGameModel model) : base(logic, model)
        {
            this.logic.Attacked += Logic_Attacked;
            this.logic.HostileDestroyed += Logic_HostileDestroyed;

            Brush ared = new SolidColorBrush(Color.FromArgb(255, 255, 59, 48));
            Brush ablue = new SolidColorBrush(Color.FromArgb(255, 0, 122, 255));

            // enemy ship
            GameObjects.Ship ks = new GameObjects.Ship(1130, 200, 640, new Uri("pack://application:,,,/LightSpeed;component/Assets/krillship.png"), () => this.model.HostileShip.ShieldCharge == 100);
            ProgressBar kh = new ProgressBar(1370, 980, 500, 50, () => this.model.HostileShip.Health, Brushes.Red);
            ProgressBar kw = new ProgressBar(1260, 570, 250, 50, () => this.model.HostileShip.WeaponsCharge, ared, "weapons");
            ProgressBar kshield = new ProgressBar(1630, 420, 250, 50, () => this.model.HostileShip.ShieldCharge, ablue, "shields");
            ks.Visible = this.InCombat;
            kh.Visible = this.InCombat;
            kw.Visible = this.InCombat;
            kshield.Visible = this.InCombat;
            this.GameObjects.AddRange(new GameObject[] { ks, kh, kw, kshield });

            // player ship
            this.PlayerHUD();
            this.GameObjects.Add(new ProgressBar(630, 820, 250, 50, () => this.model.PlayerShip.WeaponsCharge, ared, "weapons"));
            this.GameObjects.Add(new ProgressBar(200, 760, 250, 50, () => this.model.PlayerShip.ShieldCharge, ablue, "shields"));
            this.GameObjects.Add(new Button(1010, 980, 200, 50, () => "Torpedo (" + this.model.TorpedoCount + ")", this.Launch_Torpedo));

            // space pause
            Label paused = new Label(50, () => "PAUSED", Brushes.Gray, 30, FontStyles.Normal, FontWeights.Normal)
            {
                Visible = () => !this.model.IsRunning
            };
            this.GameObjects.Add(paused);

            // energy systems
            this.GameObjects.Add(new AvailableEnergy(0, 930, () => this.model.AvailableEnergy));
            double pos = 150;
            foreach (EnergySystem system in this.model.Systems)
            {
                this.GameObjects.Add(new EnergyDisplay(pos, 930, this.GetEnergyLevel, system.Max, system.Name, e => this.EnergyClick(e, system.Name)));
                pos += 150;
            }
        }


        /// <inheritdoc />
        public override void HandleKey(KeyEventArgs e)
        {
            base.HandleKey(e);
            if (e.Key == Key.Space)
            {
                this.logic.TogglePause();
            }

            if (cheat[cheatcount] == e.Key.ToString()[0])
            {
                cheatcount++;
                if (cheatcount == cheat.Length)
                {
                    cheatcount = 0;
                    this.model.TorpedoCount += 10;
                }
            }
            else
            {
                cheatcount = 0;
            }
        }

        private bool InCombat()
        {
            return this.model.State == GameStates.Combat;
        }

        private int GetEnergyLevel(string name)
        {
            return this.model.Systems.First(x => x.Name == name).Level;
        }

        private void Logic_HostileDestroyed(object sender, EventArgs e)
        {
            this.GameObjects.Add(new Explosion(1250, 250, 400));
        }

        private void Logic_Attacked(object sender, AttackEventArgs e)
        {
            if (e.Phaser)
            {
                this.GameObjects.Add(new Phaser(750, 520, e.Player));
            }
        }

        private void Launch_Torpedo(MouseEventArgs e)
        {
            if (this.model.TorpedoCount > 0 && this.model.State == GameStates.Combat)
            {
                this.logic.LaunchTorpedo();
                this.GameObjects.Add(new Torpedo(660, 730, 630, 350));
            }
        }

        private void EnergyClick(MouseEventArgs e, string system)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                this.logic.EnergyUp(system);
            }
            else
            {
                this.logic.EnergyDown(system);
            }
        }
    }
}
