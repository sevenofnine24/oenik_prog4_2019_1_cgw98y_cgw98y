﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using LightSpeed.GameObjects;
using LightSpeed.Logic;
using LightSpeed.Model;

namespace LightSpeed.Views
{
    /// <summary>
    /// Menu view
    /// </summary>
    class MenuView : View
    {
        /// <inheritdoc />
        public MenuView(IGameLogic logic, IGameModel model) : base(logic, model)
        {
            this.GameObjects.Add(new Label(270, () => "Lightspeed", Brushes.White, 42, FontStyles.Normal, FontWeights.Bold));
            this.GameObjects.Add(new Button(ViewUtility.Center(1920, 250), 390, 250, 60, () => "Play", this.Play_OnClick));
            this.GameObjects.Add(new Button(ViewUtility.Center(1920, 250), 490, 250, 60, () => "Exit", this.Exit_OnClick));
        }

        private void Exit_OnClick(MouseButtonEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Play_OnClick(MouseButtonEventArgs e)
        {
            this.logic.StartGame();
        }
    }
}
