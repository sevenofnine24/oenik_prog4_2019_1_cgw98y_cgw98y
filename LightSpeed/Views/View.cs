﻿using LightSpeed.GameObjects;
using LightSpeed.GameObjects.Animated;
using LightSpeed.Logic;
using LightSpeed.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace LightSpeed.Views
{
    abstract class View
    {
        protected IGameLogic logic;
        protected IGameModel model;
        protected ClickableGameObject hover;

        public List<GameObject> GameObjects { get; protected set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logic">Logic interface</param>
        /// <param name="model">Model interface</param>
        public View(IGameLogic logic, IGameModel model)
        {
            this.logic = logic;
            this.model = model;

            this.GameObjects = new List<GameObject>();
        }

        /// <summary>
        /// Handle key press
        /// </summary>
        /// <param name="e">Key event</param>
        public virtual void HandleKey(KeyEventArgs e)
        {
            // do nothing
        }

        /// <summary>
        /// Handle mouse move
        /// </summary>
        /// <param name="p">New position of mouse</param>
        public void HandleMouseMove(Point p)
        {
            this.hover = null;
            List<GameObject> unified = this.GameObjects;
            PausableView pv = this as PausableView;
            if (pv != null)
            {
                unified = unified.Concat(pv.PauseMenu).ToList();
            }
            foreach (GameObject o in unified)
            {
                if (o is ClickableGameObject c)
                {
                    if (c.Hitbox.IntersectsWith(new Rect(p, new Size())))
                    {
                        this.hover = c;
                        c.Hover = true;
                    }
                    else
                    {
                        c.Hover = false;
                    }
                }

            }
        }

        /// <summary>
        /// Handle mouse down
        /// </summary>
        /// <param name="e">Mouse event</param>
        public void HandleMouseDown(MouseButtonEventArgs e)
        {
            this.hover?.Clicked?.Invoke(e);
        }

        /// <summary>
        /// Renders all game objects
        /// </summary>
        /// <param name="ctx">Drawing context</param>
        public virtual void Render(DrawingContext ctx)
        {
            foreach(GameObject o in this.GameObjects)
            {
                if (o.Visible == null || o.Visible())
                {
                    o.Render(ctx);
                }
            }
        }
    }
}
