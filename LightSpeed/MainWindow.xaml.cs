﻿using LightSpeed.Logic;
using LightSpeed.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace LightSpeed
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private IGameLogic logic;
        private IGameModel model;

        /// <summary>
        /// Main window
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            this.Loaded += MainWindow_Loaded;
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            this.model = new GameModel();
            this.logic = new GameLogic(model);
            this.logic.PlaySound += Logic_PlaySound;

            this.Display.Initialize(logic, model);
            this.Display.InvalidateVisual();

            this.SizeChanged += MainWindow_SizeChanged;

            DispatcherTimer timer = new DispatcherTimer
            {
                Interval = new TimeSpan(100000)
            };
            timer.Tick += Timer_Tick;
            timer.Start();
        }

        private void Logic_PlaySound(object sender, SoundEventArgs e)
        {
            Stream sound = null;
            switch (e.Sound)
            {
                case SoundEnum.Torpedo:
                    sound = Properties.Resources.tng_torpedo;
                    break;
                case SoundEnum.SmallExplosion:
                    sound = Properties.Resources.smallexplosion;
                    break;
                case SoundEnum.LargeExplosion:
                    sound = Properties.Resources.largeexplosion;
                    break;
                case SoundEnum.Ftl:
                    sound = Properties.Resources.tng_warp;
                    break;
                case SoundEnum.PlayerPhaser:
                    sound = Properties.Resources.tng_phaser;
                    break;
                case SoundEnum.EnemyPhaser:
                    sound = Properties.Resources.tng_phaserenemy;
                    break;
            }
            SoundPlayer sp = new SoundPlayer(sound);
            sp.Play();
        }

        private void MainWindow_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.Display.UpdateVU();
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            this.Display.InvalidateVisual();
            if (this.model.IsRunning)
            {
                this.logic.Tick();
                this.Display.Tick();
            }
        }
    }
}
