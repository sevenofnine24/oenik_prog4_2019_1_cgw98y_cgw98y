﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightSpeed.Model
{
    /// <summary>
    /// Energy system object
    /// </summary>
    public class EnergySystem
    {
        /// <summary>
        /// Name of energy system
        /// </summary>
        public string Name { get; private set; }

        /// <summary>
        /// Current energy level
        /// </summary>
        public int Level { get; private set; }

        /// <summary>
        /// Maximum energy level
        /// </summary>
        public int Max { get; private set; }

        /// <summary>
        /// Constructor for energy system
        /// </summary>
        /// <param name="name">Name of system</param>
        /// <param name="max">Maximum energy level</param>
        public EnergySystem(string name, int max)
        {
            this.Name = name;
            this.Level = 1;
            this.Max = max;
        }

        /// <summary>
        /// Increase level if possible
        /// </summary>
        public void Up()
        {
            if (this.Level < this.Max)
            {
                this.Level++;
            }
        }

        /// <summary>
        /// Decrease level if possible
        /// </summary>
        public void Down()
        {
            if (this.Level > 0)
            {
                this.Level--;
            }
        }
    }
}
