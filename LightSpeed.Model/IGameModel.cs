﻿using System;
using System.Collections.Generic;

namespace LightSpeed.Model
{
    /// <summary>
    /// Game model interface
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// View is to be replaced
        /// </summary>
        event EventHandler StateChanged;

        /// <summary>
        /// Game is running (paused or not)
        /// </summary>
        bool IsRunning { get; set; }

        /// <summary>
        /// Current state
        /// </summary>
        GameStates State { get; set; }

        /// <summary>
        /// Energy systems list
        /// </summary>
        List<EnergySystem> Systems { get; }

        /// <summary>
        /// Player's ship object
        /// </summary>
        Ship PlayerShip { get; }

        /// <summary>
        /// Hostile ship object
        /// </summary>
        Ship HostileShip { get; }

        /// <summary>
        /// Calculated available energy
        /// </summary>
        int AvailableEnergy { get; }

        /// <summary>
        /// FTL drive charge
        /// </summary>
        int FtlCharge { get; set; }

        /// <summary>
        /// Number of torpedoes available
        /// </summary>
        int TorpedoCount { get; set; }

        /// <summary>
        /// How much fuel is available
        /// </summary>
        int Fuel { get; set; }

        /// <summary>
        /// Number of scrap (currency)
        /// </summary>
        int Scrap { get; set; }

        /// <summary>
        /// Shield charge multiplier
        /// </summary>
        int ShieldMultiplier { get; set; }

        /// <summary>
        /// Weapons damage multiplier
        /// </summary>
        int WeaponsMultiplier { get; set; }

        /// <summary>
        /// Current sector
        /// </summary>
        int Sector { get; set; }

        /// <summary>
        /// Restart game
        /// </summary>
        void Reconstruct();

        /// <summary>
        /// Generate a hostile ship
        /// </summary>
        void GenerateHostile();
    }
}
