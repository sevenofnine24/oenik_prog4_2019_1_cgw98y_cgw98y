﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightSpeed.Model
{
    /// <summary>
    /// Ship class
    /// </summary>
    public class Ship
    {
        private int shieldcharge;
        private int weaponscharge;

        /// <summary>
        /// Health of ship
        /// </summary>
        public int Health { get; set; }

        /// <summary>
        /// Current shield charge, 100 maximum
        /// </summary>
        public int ShieldCharge { get => this.shieldcharge; set => this.shieldcharge = value > 100 ? 100 : value; }

        /// <summary>
        /// Current weapons charge, 100 maximum
        /// </summary>
        public int WeaponsCharge { get => this.weaponscharge; set => this.weaponscharge = value > 100 ? 100 : value; }

        /// <summary>
        /// Constructor
        /// </summary>
        public Ship()
        {
            this.Health = 100;
        }

        /// <summary>
        /// Generate random ship (hostile)
        /// </summary>
        public void Randomize()
        {
            Random r = new Random();
            this.Health = r.Next(75, 101);
            this.ShieldCharge = 0;
            this.WeaponsCharge = 0;
        }
    }
}
