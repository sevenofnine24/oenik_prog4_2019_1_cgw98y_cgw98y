﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightSpeed.Model
{
    /// <summary>
    /// Game model implementation
    /// </summary>
    public class GameModel : IGameModel
    {
        private GameStates state;
        private int ftlcharge;

        /// <inheritdoc />
        public event EventHandler StateChanged;

        /// <inheritdoc />
        public bool IsRunning { get; set; }

        /// <inheritdoc />
        public Ship PlayerShip { get; private set; }

        /// <inheritdoc />
        public Ship HostileShip { get; private set; }

        /// <inheritdoc />
        public List<EnergySystem> Systems { get; private set; }

        /// <inheritdoc />
        public int AvailableEnergy => 4 - Systems.Sum(x => x.Level);

        /// <inheritdoc />
        public int Sector { get; set; }

        /// <inheritdoc />
        public int TorpedoCount { get; set; }

        /// <inheritdoc />
        public int Fuel { get; set; }

        /// <inheritdoc />
        public int Scrap { get; set; }

        /// <inheritdoc />
        public int WeaponsMultiplier { get; set; }

        /// <inheritdoc />
        public int ShieldMultiplier { get; set; }

        /// <inheritdoc />
        public GameStates State {
            get
            {
                return this.state;
            }
            set
            {
                this.state = value;
                this.PlayerShip.WeaponsCharge = 0;
                this.PlayerShip.ShieldCharge = 0;
                this.StateChanged?.Invoke(this, null);
            }
        }

        /// <inheritdoc />
        public int FtlCharge { get => this.ftlcharge; set => this.ftlcharge = this.ftlcharge = value > 100 ? 100 : value; }

        /// <inheritdoc />
        public GameModel()
        {
            this.Reconstruct();
            this.State = GameStates.Menu;
        }

        /// <inheritdoc />
        public void Reconstruct()
        {
            this.PlayerShip = new Ship();
            this.HostileShip = new Ship();

            this.Systems = new List<EnergySystem>
            {
                new EnergySystem("weapons", 3),
                new EnergySystem("shields", 3),
                new EnergySystem("engine", 3)
            };
            this.Sector = 0;
            this.FtlCharge = 0;
            this.TorpedoCount = 5;
            this.Scrap = 5;
            this.Fuel = 5;
            this.WeaponsMultiplier = 1;
            this.ShieldMultiplier = 1;
        }

        /// <inheritdoc />
        public void GenerateHostile()
        {
            HostileShip.Randomize();
        }
    }
}
