﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightSpeed.Model
{
    /// <summary>
    /// Game states (maps to views)
    /// </summary>
    public enum GameStates
    {
        /// <summary>
        /// Menu state
        /// </summary>
        Menu,
        /// <summary>
        /// Main game state
        /// </summary>
        Game,
        /// <summary>
        /// Main game state, in combat
        /// </summary>
        Combat,
        /// <summary>
        /// Trader game state
        /// </summary>
        Trade,
        /// <summary>
        /// Victory state
        /// </summary>
        Victory,
        /// <summary>
        /// Game over state
        /// </summary>
        GameOver
    }
}
