﻿using LightSpeed.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightSpeed.Logic
{
    /// <summary>
    /// Game logic implementation
    /// </summary>
    public class GameLogic : IGameLogic
    {
        private Random r;
        private int timerDivider;
        private readonly IGameModel model;
        private int prevEncounter;

        /// <inheritdoc />
        public event EventHandler<AttackEventArgs> Attacked;

        /// <inheritdoc />
        public event EventHandler<MessageEventArgs> Message;

        /// <inheritdoc />
        public event EventHandler HostileDestroyed;

        /// <inheritdoc />
        public event EventHandler<SoundEventArgs> PlaySound;

        /// <inheritdoc />
        public GameLogic(IGameModel model)
        {
            this.model = model;
            this.r = new Random();
        }

        /// <inheritdoc />
        public void EnergyDown(string system)
        {
            this.model.Systems.First(x => x.Name == system).Down();
        }

        /// <inheritdoc />
        public void EnergyUp(string system)
        {
            if (this.model.AvailableEnergy > 0)
            {
                this.model.Systems.First(x => x.Name == system).Up();
            }
        }

        /// <inheritdoc />
        public void PlayerAttack(bool isTorpedo = false)
        {
            int dmg = this.CommonAttack(this.model.PlayerShip, this.model.HostileShip, true, isTorpedo);
            this.Attacked?.Invoke(this, new AttackEventArgs(true, dmg, !isTorpedo));

            if (isTorpedo)
            {
                this.PlaySound?.Invoke(this, new SoundEventArgs(SoundEnum.SmallExplosion));
            }
            if (this.model.HostileShip.Health <= 0 && this.model.State == GameStates.Combat)
            {
                this.HostileDestroyed?.Invoke(this, null);
                this.model.FtlCharge = 100;
                this.model.State = GameStates.Game;
                this.model.PlayerShip.Health = 100;
                this.PlaySound?.Invoke(this, new SoundEventArgs(SoundEnum.LargeExplosion));
                this.RandomReward();
            }
        }

        /// <inheritdoc />
        public void LaunchTorpedo()
        {
            this.model.TorpedoCount--;
            this.PlaySound?.Invoke(this, new SoundEventArgs(SoundEnum.Torpedo));
        }

        /// <inheritdoc />
        public int HostileAttack()
        {
            int dmg = this.CommonAttack(this.model.HostileShip, this.model.PlayerShip, false);
            this.Attacked?.Invoke(this, new AttackEventArgs(false, dmg, true));

            if (this.model.PlayerShip.Health <= 0)
            {
                this.model.State = GameStates.GameOver;
            }

            return dmg;
        }

        /// <inheritdoc />
        public void JumpSector()
        {
            if (this.model.Sector == 0 || this.model.Fuel > 0 && this.model.FtlCharge == 100)
            {
                if (this.model.Sector != 0)
                {
                    this.PlaySound?.Invoke(this, new SoundEventArgs(SoundEnum.Ftl));
                }
                
                this.model.Sector++;
                this.model.Fuel--;
                this.model.FtlCharge = 0;

                // winning number
                if (this.model.Sector == 11)
                {
                    this.model.State = GameStates.Victory;
                }
                else
                {
                    // random generation of encounters here
                    int encounter = -1;
                    do
                    {
                        encounter = this.r.Next(0, 3);
                    }
                    while (encounter == this.prevEncounter);

                    this.prevEncounter = encounter;
                    if (encounter == 0)
                    {
                        // hostile
                        this.model.GenerateHostile();
                        this.model.State = GameStates.Combat;
                    }
                    else if (encounter == 1)
                    {
                        // distress
                        int rand = this.r.Next(0, 3);
                        MessageEventArgs args = null;
                        string title = "Random encounter";
                        switch (rand)
                        {
                            case 0:
                                args = new MessageEventArgs(title, "You have encountered a ship that's badly damaged,\n" +
                                    "their life support is down. Do you help?", true);
                                break;
                            case 1:
                                args = new MessageEventArgs(title, "You have encountered a distress beacon.\nDo you want " +
                                    "to salvage it for scrap?", true);
                                break;
                            case 2:
                                args = new MessageEventArgs(title, "You have found a space station in deep space.\n" +
                                    "Do you want to dock and investigate?", true);
                                break;
                        }
                        this.Message?.Invoke(this, args);
                        
                        if (args.Response)
                        {
                            int b = r.Next(0, 1);
                            switch (rand)
                            {
                                case 0:
                                    if (b == 0)
                                    {
                                        this.model.GenerateHostile();
                                        int dmg = this.HostileAttack();
                                        args = new MessageEventArgs(title, "It was a trap! The ship has attacked you, for " + dmg + " damage.");
                                        this.model.State = GameStates.Combat;
                                    }
                                    else
                                    {
                                        int scrap = r.Next(1, 4);
                                        args = new MessageEventArgs(title, "You helped the other crew repair their systems, in return, they offered you " + scrap + " fuel!");
                                        this.model.Fuel += scrap;
                                        this.model.State = GameStates.Game;
                                    }
                                    break;
                                case 1:
                                    if (b == 0)
                                    {
                                        int scrap = r.Next(6, 15);
                                        args = new MessageEventArgs(title, "You have salvaged the beacon, gaining " + scrap + " scrap.");
                                        this.model.Scrap += scrap;
                                    }
                                    else
                                    {
                                        args = new MessageEventArgs(title, "The beacon has exploded, the explosion knocked out the ship's systems," +
                                            " resulting in loss of all of your scrap.");
                                        this.model.Scrap = 0;
                                    }
                                    this.model.FtlCharge = 100;
                                    this.model.State = GameStates.Game;
                                    break;
                                case 2:
                                    if (b == 0)
                                    {
                                        args = new MessageEventArgs(title, "The space station you have encountered is a trader. Happy shopping!");
                                        this.model.State = GameStates.Trade;
                                        this.model.FtlCharge = 100;
                                    }
                                    else
                                    {
                                        if (r.Next(0, 1) == 0)
                                        {
                                            int torpedoes = r.Next(1, 4);
                                            int scrap = r.Next(5, 15);
                                            int fuel = r.Next(1, 5);
                                            args = new MessageEventArgs(title, "The space station you have encountered is abandoned, you have found " + torpedoes + " torpedoes, " +
                                                scrap + " scrap and " + fuel + " fuel!");
                                            this.model.TorpedoCount += torpedoes;
                                            this.model.Scrap += scrap;
                                            this.model.Fuel += fuel;
                                        }
                                        else
                                        {
                                            int dmg = this.HostileAttack();
                                            args = new MessageEventArgs(title, "The space station you have encountered is an enemy outpost. You managed to escape, but you took " + dmg
                                                + " damage.");
                                        }
                                        this.model.FtlCharge = 100;
                                        this.model.State = GameStates.Game;
                                    }
                                    break;
                            }
                            this.Message?.Invoke(this, args);
                        }
                        else
                        {
                            this.model.FtlCharge = 100;
                            this.model.State = GameStates.Game;
                        }

                    }
                    else
                    {
                        // trader
                        this.model.State = GameStates.Trade;
                        this.model.FtlCharge = 100;
                    }
                }
            }
        }

        /// <inheritdoc />
        public void TogglePause()
        {
            this.TogglePause(this.model.IsRunning);
        }

        /// <inheritdoc />
        public void TogglePause(bool _override)
        {
            this.model.IsRunning = !_override;
        }

        /// <inheritdoc />
        public void StartGame()
        {
            this.prevEncounter = -1;
            this.model.IsRunning = true;
            this.model.Reconstruct();
            this.model.State = GameStates.Game;
            this.JumpSector();
        }

        /// <inheritdoc />
        public void Tick()
        {
            // timer division, shields / weapons / FTL charges
            timerDivider++;
            if (timerDivider % 10 == 0)
            {
                if (this.model.State == GameStates.Combat)
                {
                    // player
                    this.model.PlayerShip.ShieldCharge += this.model.Systems.First(x => x.Name == "shields").Level * this.model.ShieldMultiplier;
                    this.model.PlayerShip.WeaponsCharge += this.model.Systems.First(x => x.Name == "weapons").Level;

                    // hostile
                    this.model.HostileShip.ShieldCharge += r.Next(1, 4);
                    this.model.HostileShip.WeaponsCharge += r.Next(1, 4);
                }

            }
            if (timerDivider % 100 == 0)
            {
                this.model.FtlCharge += this.model.Systems.First(x => x.Name == "engine").Level;
            }
            if (timerDivider == 1000)
            {
                timerDivider = 0;
            }

            // combat
            if (this.model.State == GameStates.Combat)
            {
                if (this.model.PlayerShip.WeaponsCharge == 100)
                {
                    this.PlayerAttack();
                }
                if (this.model.HostileShip.WeaponsCharge == 100)
                {
                    this.HostileAttack();
                }
            }
        }

        /// <inheritdoc />
        public void ReturnToMenu()
        {
            this.model.State = GameStates.Menu;
        }

        /// <inheritdoc />
        public void BuyFuel()
        {
            if(this.model.Scrap >= 1)
            {
                this.model.Scrap--;
                this.model.Fuel++;
            }
        }

        /// <inheritdoc />
        public void SellFuel()
        {
            if (this.model.Fuel >= 1)
            {
                this.model.Fuel--;
                this.model.Scrap++;
            }
        }

        /// <inheritdoc />
        public void BuyTorpedo()
        {
            if (this.model.Scrap >= 2)
            {
                this.model.Scrap -= 2;
                this.model.TorpedoCount++;
            }
        }

        /// <inheritdoc />
        public void SellTorpedo()
        {
            if (this.model.TorpedoCount >= 1)
            {
                this.model.TorpedoCount--;
                this.model.Scrap += 2;
            }
        }

        /// <inheritdoc />
        public void BuyWeapon()
        {
            if (this.model.Scrap >= 10)
            {
                this.model.Scrap -= 10;
                this.model.WeaponsMultiplier += 1;
            }
        }

        /// <inheritdoc />
        public void BuyShield()
        {
            if (this.model.Scrap >= 20)
            {
                this.model.Scrap -= 20;
                this.model.ShieldMultiplier += 1;
            }
        }

        private int CommonAttack(Ship attacking, Ship attacked, bool isPlayer, bool isTorpedo = false)
        {
            if (!isTorpedo)
            {
                this.PlaySound?.Invoke(this, new SoundEventArgs(isPlayer ? SoundEnum.PlayerPhaser : SoundEnum.EnemyPhaser));
            }

            attacking.WeaponsCharge = 0;
            if (attacked.ShieldCharge == 100 && !isTorpedo)
            {
                attacked.ShieldCharge = 0;
                return 0;
            }
            else
            {
                int randomDamage = this.r.Next(10, 16);

                if (isPlayer && this.model.WeaponsMultiplier > 1)
                {
                    randomDamage = randomDamage * this.model.WeaponsMultiplier;
                }

                attacked.Health -= randomDamage;
                return randomDamage;
            }
        }

        private void RandomReward()
        {
            int n = this.r.Next(1, 9);
            string type = null;

            switch (this.r.Next(0, 3))
            {
                case 0:
                    type = "fuel";
                    this.model.Fuel += n;
                    break;
                case 1:
                    type = "scrap";
                    this.model.Scrap += n;
                    break;
                case 2:
                    type = "torpedoes";
                    this.model.TorpedoCount += n;
                    break;
            }

            string msg = string.Format("You have received {0} {1}.", n, type);
            this.Message?.Invoke(this, new MessageEventArgs("Reward", msg));
        }
    }
}
