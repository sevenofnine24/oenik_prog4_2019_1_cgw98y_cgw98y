﻿using System;

namespace LightSpeed.Logic
{
    /// <summary>
    /// Attack event arguments
    /// </summary>
    public class AttackEventArgs : EventArgs
    {
        /// <summary>
        /// Is the player attacking
        /// </summary>
        public bool Player { get; private set; }

        /// <summary>
        /// Damage inflicted
        /// </summary>
        public int Damage { get; private set; }

        /// <summary>
        /// Is the weapon a phaser
        /// </summary>
        public bool Phaser { get; protected set; }


        /// <summary>
        /// Constructor for event args
        /// </summary>
        /// <param name="player">Is the player attacking</param>
        /// <param name="dmg">Damage inflicted</param>
        /// <param name="phaser">Is the weapon a phaser</param>
        public AttackEventArgs(bool player, int dmg, bool phaser)
        {
            this.Player = player;
            this.Damage = dmg;
            this.Phaser = phaser;
        }
    }
}
