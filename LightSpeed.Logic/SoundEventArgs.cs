﻿using System;

namespace LightSpeed.Logic
{
    /// <summary>
    /// Sound event args
    /// </summary>
    public class SoundEventArgs : EventArgs
    {
        /// <summary>
        /// Which sound
        /// </summary>
        public SoundEnum Sound { get; set; }

        /// <summary>
        /// Constructor for sound event args
        /// </summary>
        /// <param name="sound">Which sound</param>
        public SoundEventArgs(SoundEnum sound)
        {
            Sound = sound;
        }
    }
}
