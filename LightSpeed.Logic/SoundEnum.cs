﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LightSpeed.Logic
{
    /// <summary>
    /// Sounds to be played
    /// </summary>
    public enum SoundEnum
    {
        /// <summary>
        /// Torpedo launch sound
        /// </summary>
        Torpedo,
        /// <summary>
        /// Player's phaser sound
        /// </summary>
        PlayerPhaser,
        /// <summary>
        /// Enemy phaser sound
        /// </summary>
        EnemyPhaser,
        /// <summary>
        /// Small explosion sound (torpedo)
        /// </summary>
        SmallExplosion,
        /// <summary>
        /// Large explosion sound (ship)
        /// </summary>
        LargeExplosion,
        /// <summary>
        /// FTL drive sound (jump)
        /// </summary>
        Ftl
    }
}
