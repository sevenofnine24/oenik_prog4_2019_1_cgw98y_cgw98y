﻿using System;

namespace LightSpeed.Logic
{
    /// <summary>
    /// Message event args
    /// </summary>
    public class MessageEventArgs : EventArgs
    {
        /// <summary>
        /// Title of message box
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// Message of message box
        /// </summary>
        public string Message { get; private set; }

        /// <summary>
        /// Is it a yes / no choice
        /// </summary>
        public bool Choice { get; private set; }

        /// <summary>
        /// Response, if it's a choice
        /// </summary>
        public bool Response { get; set; }

        /// <summary>
        /// Constructor for message event args
        /// </summary>
        /// <param name="title">Title of message box</param>
        /// <param name="message">Message of message box</param>
        /// <param name="choice">Is it a yes / no choice</param>
        public MessageEventArgs(string title, string message, bool choice = false)
        {
            this.Title = title;
            this.Message = message;
            this.Choice = choice;
        }
    }
}
