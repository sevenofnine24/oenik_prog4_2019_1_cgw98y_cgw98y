﻿using System;

namespace LightSpeed.Logic
{
    /// <summary>
    /// Game logic interface
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// Event fired when a ship is attacked
        /// </summary>
        event EventHandler<AttackEventArgs> Attacked;

        /// <summary>
        /// Event fired to show users a message
        /// </summary>
        event EventHandler<MessageEventArgs> Message;

        /// <summary>
        /// Event fired when a hostile ship is destroyed
        /// </summary>
        event EventHandler HostileDestroyed;

        /// <summary>
        /// Event fired to play a sound
        /// </summary>
        event EventHandler<SoundEventArgs> PlaySound;

        #region general
        /// <summary>
        /// (Re)start the game
        /// </summary>
        void StartGame();

        /// <summary>
        /// Return to menu view
        /// </summary>
        void ReturnToMenu();

        /// <summary>
        /// Main window timer calls this to advance state
        /// </summary>
        void Tick();
        #endregion

        #region ship
        /// <summary>
        /// Jump to the next sector
        /// </summary>
        void JumpSector();

        /// <summary>
        /// Set energy level up, if possible
        /// </summary>
        /// <param name="system">System to modify</param>
        void EnergyUp(string system);

        /// <summary>
        /// Set energy level down, if possible
        /// </summary>
        /// <param name="system">System to modify</param>
        void EnergyDown(string system);

        /// <summary>
        /// Pause or unpause game
        /// </summary>
        void TogglePause();

        /// <summary>
        /// Pause or unpause game
        /// </summary>
        /// <param name="_override">Value for pause</param>
        void TogglePause(bool _override);
        #endregion

        #region trade
        /// <summary>
        /// Buy one fuel, if possible
        /// </summary>
        void BuyFuel();

        /// <summary>
        /// Sell one fuel, if possible
        /// </summary>
        void SellFuel();

        /// <summary>
        /// Buy a torpedo, if possible
        /// </summary>
        void BuyTorpedo();

        /// <summary>
        /// Sell a torpedo, if possible
        /// </summary>
        void SellTorpedo();

        /// <summary>
        /// Buy a weapon upgrade, if possible
        /// </summary>
        void BuyWeapon();

        /// <summary>
        /// Buy a shield upgrade, if possible
        /// </summary>
        void BuyShield();
        #endregion

        #region combat
        /// <summary>
        /// Player attacks
        /// </summary>
        /// <param name="isTorpedo">Phaser or torpedo</param>
        void PlayerAttack(bool isTorpedo = false);

        /// <summary>
        /// Hostile attacks
        /// </summary>
        /// <returns>Damage taken</returns>
        int HostileAttack();

        /// <summary>
        /// Launch a torpedo
        /// </summary>
        void LaunchTorpedo();
        #endregion
    }
}
