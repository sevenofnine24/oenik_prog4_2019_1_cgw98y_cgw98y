var searchData=
[
  ['health',['Health',['../class_light_speed_1_1_model_1_1_ship.html#a6fe84890919387d94a0c74062655b72f',1,'LightSpeed::Model::Ship']]],
  ['hitbox',['Hitbox',['../class_light_speed_1_1_game_objects_1_1_clickable_game_object.html#a94c5aa95fcebac3100548bc5f304590b',1,'LightSpeed::GameObjects::ClickableGameObject']]],
  ['hostileship',['HostileShip',['../class_light_speed_1_1_model_1_1_game_model.html#a689592f2ae72eefc3e605af270e4d94b',1,'LightSpeed.Model.GameModel.HostileShip()'],['../interface_light_speed_1_1_model_1_1_i_game_model.html#acc0aeb927bc5cfe241fb229f4fe1275b',1,'LightSpeed.Model.IGameModel.HostileShip()']]],
  ['hover',['Hover',['../class_light_speed_1_1_game_objects_1_1_clickable_game_object.html#a1d30c37fff412e4f2088c8d67a1df444',1,'LightSpeed::GameObjects::ClickableGameObject']]]
];
