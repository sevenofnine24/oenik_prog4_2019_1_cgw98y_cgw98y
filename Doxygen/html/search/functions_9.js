var searchData=
[
  ['label',['Label',['../class_light_speed_1_1_game_objects_1_1_label.html#aafdfb6a1d5f933ccb7edbbff990ccaec',1,'LightSpeed.GameObjects.Label.Label(double x, double y, Func&lt; string &gt; content, Brush color, int size, FontStyle style, FontWeight weight)'],['../class_light_speed_1_1_game_objects_1_1_label.html#a28eb2ee65b258579f536291db2583451',1,'LightSpeed.GameObjects.Label.Label(double y, Func&lt; string &gt; content, Brush color, int size, FontStyle style, FontWeight weight, double x=0, double width=1920)']]],
  ['launchtorpedo',['LaunchTorpedo',['../class_light_speed_1_1_logic_1_1_game_logic.html#a9f2a40989657a37bcd12e18bcdb0fd63',1,'LightSpeed.Logic.GameLogic.LaunchTorpedo()'],['../interface_light_speed_1_1_logic_1_1_i_game_logic.html#a05e382d6a92110616ae66ff017382f35',1,'LightSpeed.Logic.IGameLogic.LaunchTorpedo()']]]
];
